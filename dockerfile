FROM centos:7

RUN yum install python3 python3-pip -y
RUN pip3 install flask flask_restful

copy python-api.py /opt/python-api/python-api.py
CMD ["python3" "/opt/python-api/python-api.py"]
